using System.Collections.Generic;

public class SceneToDB
{
    public string sceneType;
    public List<Object> sceneObjects;

    public SceneToDB(List<Object> objects, string sceneType)
    {
        this.sceneType = sceneType;
        this.sceneObjects = objects;
    }
}
