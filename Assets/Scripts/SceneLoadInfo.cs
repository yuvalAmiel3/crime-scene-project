using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoadInfo : MonoBehaviour
{
    private string sceneDescription;

    public SceneLoadInfo(string info)
    {
        this.sceneDescription = info;
    }

    public string getDescription()
    {
        return this.sceneDescription;
    }
}
