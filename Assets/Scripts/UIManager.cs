﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    //Screen object variables
    public GameObject loginUI;
    public GameObject registerUI;

    public InputField emailInput;
    public InputField passwordInput;
    public InputField verifyPassword;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    //Functions to change the login screen UI
    public void LoginScreen() //Back button
    {
        EraseFields();
        loginUI.SetActive(true);
        registerUI.SetActive(false);
    }
    public void RegisterScreen() // Register button
    {
        EraseFields();
        loginUI.SetActive(false);
        registerUI.SetActive(true);
    }

    private void EraseFields()
    {
        emailInput.text = "";
        passwordInput.text = "";
        verifyPassword.text = "";
    }
}
