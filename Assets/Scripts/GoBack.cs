using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoBack : MonoBehaviour
{
    public GameObject newSceneButton, loadSceneButton, backButton, 
        scrollViewLoadScene, scrollViewNewScene;

    public void Click()
    {
        backButton.SetActive(false);
        newSceneButton.SetActive(true);
        loadSceneButton.SetActive(true);
        
        GameObject activeScrollView = GameObject.FindGameObjectWithTag("NewSceneScrollView");
        if (activeScrollView != null)
            activeScrollView.SetActive(false);
        else
            GameObject.FindGameObjectWithTag("LoadSceneScrollView").SetActive(false);
    }

    public void setBackButton()
    {
        backButton.SetActive(true);
        newSceneButton.SetActive(false);
        loadSceneButton.SetActive(false);
        //scrollViewLoadScene.SetActive(true);
    }
}
