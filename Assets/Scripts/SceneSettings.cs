using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSettings : MonoBehaviour
{
    private bool isSceneLoaded = false;
    private string sceneName;

    public void sceneFromDB(bool set, string name)
    {
        this.isSceneLoaded = set;
        this.sceneName = name;
    }

    public bool getIsSceneLoaded()
    {
        return this.isSceneLoaded;
    }

    public string getSceneName()
    {
        return this.sceneName;
    }
}
