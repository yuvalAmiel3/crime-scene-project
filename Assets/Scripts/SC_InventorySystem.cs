using UnityEngine;

public class SC_InventorySystem : MonoBehaviour
{
    public Texture crosshairTexture;
    public SC_CharacterController playerController;
    public SC_PickItem[] availableItems; //List with Prefabs of all the available items

    //Available items slots
    int[] itemSlots = new int[11];
    bool showInventory = false;
    float windowAnimation = 1;
    float animationTimer = 0;

    //UI Drag & Drop
    int hoveringOverIndex = -1;
    int itemIndexToDrag = -1;
    Vector2 dragOffset = Vector2.zero;

    //Item Pick up
    DragObject itemDragObject;
    SC_PickItem detectedItem;
    int detectedItemIndex;

    SavedScenes m_SavedScenes;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        m_SavedScenes = FindObjectOfType<SavedScenes>();
        //Initialize Item Slots
        for (int i = 0; i < itemSlots.Length; i++)
            itemSlots[i] = i;
    }

    void HandleTabKey()
    {
        showInventory = !showInventory;
        animationTimer = 0;

        if (showInventory)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    private void pauseScene()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        playerController.canMove = false;
        Debug.Log("inside pause function");
    }

    public void continueScene()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        playerController.canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Show/Hide inventory
        if (Input.GetKeyDown(KeyCode.Tab))
            HandleTabKey();

        if(Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.H))
            HandleSaveKey();

        if (animationTimer < 1)
            animationTimer += Time.deltaTime;

        if (showInventory)
        {
            windowAnimation = Mathf.Lerp(windowAnimation, 0, animationTimer);
            playerController.canMove = false;
        }
        else if(!Input.GetKey(KeyCode.Mouse1))
        {
            windowAnimation = Mathf.Lerp(windowAnimation, 1f, animationTimer);
            playerController.canMove = true;
        }
        else
            playerController.canMove = false;

        //Begin item drag
        if (Input.GetMouseButtonDown(0) && hoveringOverIndex > -1 && itemSlots[hoveringOverIndex] > -1)
        {
            itemIndexToDrag = hoveringOverIndex;
        }

        //Release selected item from the inventory
        if (Input.GetMouseButtonUp(0) && itemIndexToDrag > -1)
        {
            //Release item of out the inventory
            if (hoveringOverIndex < 0)
                Instantiate(availableItems[itemSlots[itemIndexToDrag]], 
                    playerController.playerCamera.transform.position + 
                    (playerController.playerCamera.transform.forward), Quaternion.identity);
            
            //Release item inside the inventory 
            else
            {
                //Switch items between the selected slot and the one we are hovering on
                int itemIndexTmp = itemSlots[itemIndexToDrag];
                itemSlots[itemIndexToDrag] = itemSlots[hoveringOverIndex];
                itemSlots[hoveringOverIndex] = itemIndexTmp;
            }
            itemIndexToDrag = -1;
        }

        //Item pick up
        if (detectedItem && detectedItemIndex > -1)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                detectedItem.PickItem();
            }
        }
    }

    private void HandleSaveKey()
    {
        Debug.Log("inside handle save key");
        m_SavedScenes.Show();
        pauseScene();
    }

    void FixedUpdate()
    {
        if (!Cursor.visible)
        {
            //Detect if the Player is looking at any item
            RaycastHit hit;
            Ray ray = playerController.playerCamera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));

            if (Physics.Raycast(ray, out hit, 2.5f))
            {
                Transform objectHit = hit.transform;

                if (objectHit.CompareTag("Respawn"))
                {
                    if ((detectedItem == null || detectedItem.transform != objectHit) && objectHit.GetComponent<SC_PickItem>() != null)
                    {
                        SC_PickItem itemTmp = objectHit.GetComponent<SC_PickItem>();
                        DragObject tmpObj = objectHit.GetComponent<DragObject>();

                        //Check if item is in availableItemsList
                        for (int i = 0; i < availableItems.Length; i++)
                        {
                            if (availableItems[i].itemName == itemTmp.itemName)
                            {
                                detectedItem = itemTmp;
                                itemDragObject = tmpObj;
                                detectedItemIndex = i;
                            }
                        }
                    }
                }
                else
                {
                    detectedItem = null;
                }
            }
            else
            {
                detectedItem = null;
            }
        }
    }

    void OnGUI()
    {
        //Inventory UI
        GUI.Label(new Rect(5, 5, 200, 25), "Press 'Tab' to open Inventory");

        //SaveScene UI
        GUI.Label(new Rect(1660, 5, 300, 200), "Press 'Ctrl + H' to save your Crime Scene");
        
        //Inventory window
        if (windowAnimation < 1)
        {
            GUILayout.BeginArea(new Rect(5 - (430 * windowAnimation), Screen.height / 2 - 200, 350, 500), GUI.skin.GetStyle("box"));

            GUILayout.Label("Inventory", GUILayout.Height(25));

            GUILayout.BeginVertical();
            for (int i = 0; i < itemSlots.Length; i += 3)
            {
                GUILayout.BeginHorizontal();
                //Display 3 items in a row
                for (int a = 0; a < 3; a++)
                {
                    if (i + a < itemSlots.Length)
                    {
                        if (itemIndexToDrag == i + a || (itemIndexToDrag > -1 && hoveringOverIndex == i + a))
                        {
                            GUI.enabled = false;
                        }

                        if (itemSlots[i + a] > -1)
                        {
                            if (availableItems[itemSlots[i + a]].itemPreview)
                            {
                                GUILayout.Box(availableItems[itemSlots[i + a]].itemPreview, GUILayout.Width(110), GUILayout.Height(110));
                            }
                            else
                            {
                                GUILayout.Box(availableItems[itemSlots[i + a]].itemName, GUILayout.Width(110), GUILayout.Height(110));
                            }
                        }
                        else
                        {
                            //Empty slot
                            GUILayout.Box("", GUILayout.Width(110), GUILayout.Height(110));
                        }

                        //Detect if the mouse cursor is hovering over item
                        Rect lastRect = GUILayoutUtility.GetLastRect();
                        Vector2 eventMousePositon = Event.current.mousePosition;
                        if (Event.current.type == EventType.Repaint && lastRect.Contains(eventMousePositon))
                        {
                            hoveringOverIndex = i + a;
                            if (itemIndexToDrag < 0)
                            {
                                dragOffset = new Vector2(lastRect.x - eventMousePositon.x, lastRect.y - eventMousePositon.y);
                            }
                        }

                        GUI.enabled = true;
                    }
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();

            if (Event.current.type == EventType.Repaint && !GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
            {
                hoveringOverIndex = -1;
            }

            GUILayout.EndArea();
        }

        //Item dragging
        if (itemIndexToDrag > -1)
        {
            if (availableItems[itemSlots[itemIndexToDrag]].itemPreview)
            {
                GUI.Box(new Rect(Input.mousePosition.x + dragOffset.x, Screen.height - Input.mousePosition.y + dragOffset.y, 95, 95), availableItems[itemSlots[itemIndexToDrag]].itemPreview);
            }
            else
            {
                GUI.Box(new Rect(Input.mousePosition.x + dragOffset.x, Screen.height - Input.mousePosition.y + dragOffset.y, 95, 95), availableItems[itemSlots[itemIndexToDrag]].itemName);
            }
        }

        //Display the item name when hovering over it
        if (hoveringOverIndex > -1 && itemSlots[hoveringOverIndex] > -1 && itemIndexToDrag < 0)
        {
            GUI.Box(new Rect(Input.mousePosition.x, Screen.height - Input.mousePosition.y - 30, 100, 25), availableItems[itemSlots[hoveringOverIndex]].itemName);
        }

        if (!showInventory)
        {
            //Player crosshair
            GUI.color = detectedItem ? Color.green : Color.white;
            GUI.DrawTexture(new Rect(Screen.width / 2 - 4, Screen.height / 2 - 4, 8, 8), crosshairTexture);
            GUI.color = Color.white;

            //Pick up message
            if (detectedItem)
            {
                GUI.color = new Color(0, 0, 0, 0.84f);
                GUI.Label(new Rect(Screen.width / 2 - 75 + 1, Screen.height / 2 - 50 + 1, 150, 20), "Press 'F' to pick '" + detectedItem.itemName + "'");
                GUI.color = Color.green;
                GUI.Label(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 50, 150, 20), "Press 'F' to pick '" + detectedItem.itemName + "'");
                Debug.Log(detectedItem.itemName);
                if (Input.GetKey(KeyCode.Mouse1))
                {
                    itemDragObject.OnRightClickMouse();
                    playerController.canMove = false;
                }
            }
        }
    }
}