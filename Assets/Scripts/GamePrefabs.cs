using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePrefabs : MonoBehaviour
{
    public static GamePrefabs instance;
    public SC_PickItem[] myPrefabs;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public SC_PickItem[] getPrefabs()
    {
        return myPrefabs;
    }
}
