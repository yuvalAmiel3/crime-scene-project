using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CrimeScenesList : MonoBehaviour
{
    public GameObject content, myScrollView;
    public GameObject sceneInfoPrefab;
    private List<string> sceneNames;
    //private GameObject[] m_Buttons, m_Back;

    public void Awake()
    {
        sceneNames = new List<string>();
        sceneNames.Add("HouseCrimeScene");
        sceneNames.Add("EmptyAreaScene");
    }

    public void InitCrimeSceneButtons()
    {
        myScrollView.SetActive(true);
        FindObjectOfType<GoBack>().setBackButton();
        if (content.transform.childCount != 0)
            return;
        for (int i = 0; i < sceneNames.Count; i++)
        {
            GameObject prefabUI = Instantiate(sceneInfoPrefab);
            prefabInit(prefabUI, i);
        }
    }

    private void prefabInit(GameObject prefabUI, int index)
    {
        //string sceneName = sceneOptions[index].sceneName();
        string sceneName = sceneNames[index];
        prefabUI.transform.SetParent(content.transform);

        prefabUI.transform.GetChild(0).GetComponent<Text>().text = sceneName;

        prefabUI.transform.GetComponent<Button>().onClick.AddListener(delegate { LoadSceneToUnity(sceneName);});
    }

    private void LoadSceneToUnity(string sceneName)
    {
        //StartCoroutine(LoadAsyncScene(sceneName));


        SceneManager.LoadScene(sceneName);
    }

}
