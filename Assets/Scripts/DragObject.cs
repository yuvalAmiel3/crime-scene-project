using UnityEngine;

public class DragObject : MonoBehaviour
{
    private Vector3 mOffset;


    private float mZCoord;
    bool m_InCollision = false;
    Camera m_mainCamera;

    private Rigidbody preFabRigidbody;

    private void Start()
    {
        preFabRigidbody = this.GetComponent<Rigidbody>();
        m_mainCamera = Camera.main;
    }
    void OnMouseDown()

    {
        mZCoord = m_mainCamera.WorldToScreenPoint(
            gameObject.transform.position).z;

        mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
    }



    private Vector3 GetMouseAsWorldPoint()

    {

        // Pixel coordinates of mouse (x,y)

        Vector3 mousePoint = Input.mousePosition;



        // z coordinate of game object on screen

        mousePoint.z = mZCoord;



        // Convert it to world points

        return Camera.main.ScreenToWorldPoint(mousePoint);

    }

    private void Update()
    {
        /*if (Input.GetKey(KeyCode.Mouse1))
        {
            OnRightClickMouse();
        }*/
    }

    public void OnRightClickMouse()
    {
        float h = 1 * Input.GetAxis("Mouse X");
        float v = 1 * Input.GetAxis("Mouse Y");
        v /= 10;
        transform.localScale += new Vector3(v, v, v);
    }

    void OnMouseDrag()

    {
        //  if (m_InCollision)
        //     return;
        transform.position = new Vector3(GetMouseAsWorldPoint().x + mOffset.x, transform.position.y, GetMouseAsWorldPoint().z + mOffset.z);
        float h = 1 * Input.GetAxis("Mouse X");
        transform.Rotate(0, h, 0);
    }
}
