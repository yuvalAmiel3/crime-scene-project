using UnityEngine;

public class SC_PickItem : MonoBehaviour
{
    public string itemName = "Some Item"; //Each item must have a unique name
    public Texture itemPreview; // Inventory Texture of that item
    

    void Start()
    {
        //Change item tag to Respawn to detect when we look at it
        gameObject.tag = "Respawn";
    }

    public void PickItem()
    {
        Destroy(gameObject); // When picking up, Destroy the object
    }

    public Object ReturnClass()
    {
         return new Object(this.itemName,
             this.transform.position.x, this.transform.position.y, this.transform.position.z,
             this.transform.rotation.eulerAngles.x, this.transform.rotation.eulerAngles.y,
             this.transform.rotation.eulerAngles.z, this.transform.localScale.x,
             this.transform.localScale.y, this.transform.localScale.z);
    }
}

public class Object
{
    public string name;
    public float positionX, positionY, positionZ;
    public float rotationX, rotationY, rotationZ;
    public float scaleX, scaleY, scaleZ;

    public Object(string name, float positionX, float positionY, float positionZ,
        float rotationX, float rotationY, float rotationZ,
        float scaleX, float scaleY, float scaleZ)
    {
        this.name = name;
        this.positionX = positionX;
        this.positionY = positionY;
        this.positionZ = positionZ;
        this.rotationX = rotationX;
        this.rotationY = rotationY;
        this.rotationZ = rotationZ;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.scaleZ = scaleZ;
    }
}