using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SavedScenes : MonoBehaviour
{
    public GameObject m_inputWindow;
    public GameObject m_input;
    private string m_UserInput;
    public SC_InventorySystem m_Scene;
    public GameObject m_OverwriteWindow;
    public SC_CharacterController control;

    public void Start()
    {
        m_Scene = FindObjectOfType<SC_InventorySystem>();
        control = FindObjectOfType<SC_CharacterController>();
    }

    public void Hide()
    {
        if(this.m_OverwriteWindow.activeInHierarchy)
            this.m_OverwriteWindow.SetActive(false);
        else
            this.m_inputWindow.SetActive(false);
        m_Scene.continueScene();
    }

    public void Show()
    {
        Debug.Log("inside show function");
        if (FindObjectOfType<SceneSettings>().getIsSceneLoaded())
        {
            //open another window that says "are you sure you want to override "scenename"?
            //yes or no.
            //if yes, then go to saveobjects with the string from class(overrides)
            this.m_OverwriteWindow.SetActive(true);
            return;
        }
        this.m_inputWindow.SetActive(true);
    }

    public void Save()
    {
        Debug.Log("in save");
        control.canMove = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.None;
        
        //it it's an existing scene
        if (this.m_OverwriteWindow.activeInHierarchy)
        {
            PlayFabManager.instance.SaveObjects
                (FindObjectOfType<SceneSettings>().getSceneName());
            Hide();
            return;
        }

        //it it's a new scene, get the scene name and save it
        m_UserInput = m_input.GetComponent<TMP_Text>().text;
        PlayFabManager.instance.SaveObjects(m_UserInput);
        Hide();
    }
}
