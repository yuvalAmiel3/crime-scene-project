using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;
using System.Collections;

public class DataBaseList : MonoBehaviour
{

    public static DataBaseList instance;
    public List<SceneLoadInfo> scenesList;
    public GameObject sceneInfoPrefab;
    private GetUserDataResult resultUI;
    public GameObject content;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        scenesList = new List<SceneLoadInfo>();
    }

    public void setScenesList(GetUserDataResult result)
    {
        foreach (KeyValuePair<string, UserDataRecord> savedScene in result.Data)
        {
            scenesList.Add(new SceneLoadInfo(savedScene.Key));
        }
        resultUI = result;
        setToUI();
    }

    private void setToUI()
    {
        if (content.transform.childCount != 0)
            return;
        for (int i = 0; i < scenesList.Count; i++)
        {
            GameObject prefabUI = Instantiate(sceneInfoPrefab);
            prefabInit(prefabUI, i);
        }
    }

    private void prefabInit(GameObject prefabUI, int index)
    {
        prefabUI.transform.SetParent(content.transform);

        prefabUI.transform.GetChild(0).GetComponent<Text>().text += 
                                    scenesList[index].getDescription();

        prefabUI.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate {LoadSceneToUnity(index);});
    }

    public void LoadSceneToUnity(int index)
    {
        Debug.Log("in LoadSceneToUnity");
        if (resultUI.Data != null)
        {
            SceneToDB sceneInfo = JsonConvert.DeserializeObject<SceneToDB>
                (resultUI.Data[scenesList[index].getDescription()].Value);

            SC_PickItem[] myPreFabs = FindObjectOfType<GamePrefabs>().getPrefabs();
            SceneManager.LoadScene(sceneInfo.sceneType);

            if (SceneManager.GetActiveScene().name != sceneInfo.sceneType)
            {
                StartCoroutine(waitForSceneLoad
                    (sceneInfo, myPreFabs, scenesList[index].getDescription()));
            }
        }
    }

    IEnumerator waitForSceneLoad(SceneToDB sceneInfo, SC_PickItem[] myPreFabs, string sceneName)
    {
        while (SceneManager.GetActiveScene().name != sceneInfo.sceneType)
            yield return null;
        
        // Do anything after proper scene has been loaded
        if (SceneManager.GetActiveScene().name == sceneInfo.sceneType)
            LoadScenePrefabs(sceneInfo, myPreFabs, sceneName);
    }

    private void LoadScenePrefabs(SceneToDB sceneInfo, SC_PickItem[] myPreFabs, string sceneName)
    {
        FindObjectOfType<SceneSettings>().sceneFromDB(true, sceneName);
        foreach (Object obj in sceneInfo.sceneObjects)
        {
            for (int i = 0; i < myPreFabs.Length; i++)
            {
                if (obj.name == myPreFabs[i].itemName)
                {
                    SC_PickItem spawned = Instantiate(myPreFabs[i],
                       new Vector3(obj.positionX, obj.positionY, obj.positionZ)
                      , Quaternion.Euler(obj.rotationX, obj.rotationY, obj.rotationZ));
                    spawned.transform.localScale = new Vector3(obj.scaleX, obj.scaleY, obj.scaleZ);
                }
            }
        }
    }
}
