using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;
using Newtonsoft.Json;
using TMPro;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayFabManager : MonoBehaviour
{
    public static PlayFabManager instance;

    public Text messageText;
    public InputField emailInput;
    public InputField passwordInput;
    public InputField verifyPassword;
    public UIManager m_ManageUIScreens;
    public SceneSwitch m_SceneManager;
    private GameObject[] m_TextMessage;
    public GameObject myScrollView;


    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void RegisterButton()
    {
        if (passwordInput.text.Length < 6 || verifyPassword.text.Length < 6)
        {
            messageText.text = "Password too short!";
            return;
        }

        if(passwordInput.text != verifyPassword.text)
        {
            messageText.text = "Passwords don't match!";
            return;
        }

        var request = new RegisterPlayFabUserRequest
        {
            Email = emailInput.text,
            Password = passwordInput.text,
            RequireBothUsernameAndEmail = false
        };
        PlayFabClientAPI.RegisterPlayFabUser(request, OnRegisterSuccess, OnError);
    }

    void OnRegisterSuccess(RegisterPlayFabUserResult result)
    {
        messageText.text = "";
        m_ManageUIScreens.LoginScreen();
    }
    public void LoginButton()
    {
        if (passwordInput.text.Length < 6)
        {
            messageText.text = "Password too short!";
            return;
        }

        var request = new LoginWithEmailAddressRequest
        {
            Email = emailInput.text,
            Password = passwordInput.text
        };
        PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnError);
    }

    public void GetObjects()
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest(), OnObjectsDataRecieved, OnError);
    }

    void OnObjectsDataRecieved(GetUserDataResult result)
    {
        if (result.Data.Count == 0) {
            m_TextMessage = GameObject.FindGameObjectsWithTag("TextError");
            m_TextMessage[0].GetComponent<TMP_Text>().text = "No Saved Scenes Were Found";
            return;
        }

        // m_Buttons = GameObject.FindGameObjectsWithTag("CreateNLoad");
        // m_Buttons[0].SetActive(false);

        if (myScrollView == null)
            myScrollView = GameObject.FindGameObjectWithTag("LoadSceneScrollView");

        myScrollView.SetActive(true);
        FindObjectOfType<GoBack>().setBackButton();

        FindObjectOfType<DataBaseList>().setScenesList(result);
    }

    IEnumerator waitForSceneLoad(SceneToDB sceneInfo, SC_PickItem[] myPreFabs, string sceneName)
    {
        while (SceneManager.GetActiveScene().name != sceneInfo.sceneType)
            yield return null;

        // Do anything after proper scene has been loaded
        if (SceneManager.GetActiveScene().name == sceneInfo.sceneType)
            LoadScenePrefabs(sceneInfo, myPreFabs, sceneName);
    }

    private void LoadScenePrefabs(SceneToDB sceneInfo, SC_PickItem[] myPreFabs, string sceneName)
    {
        FindObjectOfType<SceneSettings>().sceneFromDB(true, sceneName);
        foreach (Object obj in sceneInfo.sceneObjects)
        {
            for (int i = 0; i < myPreFabs.Length; i++)
            {
                if (obj.name == myPreFabs[i].itemName)
                {
                    Instantiate(myPreFabs[i],
                       new Vector3(obj.positionX, obj.positionY, obj.positionZ)
                      , Quaternion.Euler(obj.rotationX, obj.rotationY, obj.rotationZ));
                }
            }
        }
    }

    void OnLoginSuccess(LoginResult result)
    {
        m_SceneManager.NextScene();
    }
    public void ResetPasswordButton()
    {
        var request = new SendAccountRecoveryEmailRequest
        {
            Email = emailInput.text,
            TitleId = "DDCEF"
        };

        PlayFabClientAPI.SendAccountRecoveryEmail(request, OnResetSuccess, OnError);
    }

    void OnResetSuccess(SendAccountRecoveryEmailResult result)
    {
        messageText.text = "Password reset mail sent!";
    }

    void OnSuccess(LoginResult result)
    {
        Debug.Log("successful login/account created");
    }

    void OnError(PlayFabError error)
    {
        messageText.text = error.ErrorMessage;
        Debug.Log(error.GenerateErrorReport());
    }

    /*
     * Saving all scene prefabs on JSON to the database
     */
    public void SaveObjects(string titleScene)
    {
        List<Object> objects = new List<Object>();

        //Getting all objects with the tag "Respawn"
        GameObject[] respawns = GameObject.FindGameObjectsWithTag("Respawn");

        //Relavent data to JSON
        foreach (var obj in respawns)
        {
            objects.Add(obj.GetComponent<SC_PickItem>().ReturnClass());
        }

        SceneToDB myDBInfo = new SceneToDB(objects, SceneManager.GetActiveScene().name);

        jsonToDB(titleScene, myDBInfo);
    }

    private void jsonToDB(string titleScene, SceneToDB sceneInfo)
    {
        var request = new UpdateUserDataRequest
        {
            Data = new Dictionary<string, string>
            {
                {titleScene, JsonConvert.SerializeObject(sceneInfo, Formatting.Indented,
                new JsonSerializerSettings() {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                 })}
            }
        };
        PlayFabClientAPI.UpdateUserData(request, OnDataSend, OnError);
    }

    void OnDataSend(UpdateUserDataResult result)
    {
        Debug.Log("successfully sent");
        SceneManager.LoadScene("EntryScene");
    }
}
